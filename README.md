<img src="https://codeberg.org/repo-avatars/40839-498014374f2617811329845f0327f6cc" alt="Ícone" width="80px">

# mofo

Easy template copying

## Get it

* Copy [mofo](bin/mofo) to some directory in your `$PATH`.
* Copy [mofo.1p](man/man1/mofo.1p) to some directory in
your `$MANPATH` (I recommend you create a manpages tree
somewhere inside your `$HOME` directory, if you haven't
already).
* Create `.config/mofo/templates` directory under your `$HOME` directory.
* Create your template files inside this new `templates` directory.

## Learn it

Considering you got the man page, as stated above:

```
man mofo
```

## Use it

Copy *mytemplate* to a new file called *mynamedfile*:

```
mofo mytemplate mynamedfile
```

If you omit the second parameter, you'll be prompted for the desired name. Leave it blank if you just want to use the same name as the template file.

If you omit both parameters, you'll also be prompted for the template filename. Use `Ctrl+D` to list options.
